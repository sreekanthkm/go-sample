package bootstrapper

import (
	"fmt"
	"os"

	"gopkg.in/mgo.v2"
)

var session *mgo.Session

func GetSession() *mgo.Session {
	if session != nil {
		return session
	}
	var err error
	di := &mgo.DialInfo{
		Addrs: []string{AppConfig.MongoHost},
	}
	session, err = mgo.DialWithInfo(di)
	if err != nil {
		fmt.Println("Unable to create mongo session", err)
	}
	return session
}

func initializeDatabase() {
	var err error
	di := &mgo.DialInfo{
		Addrs: []string{AppConfig.MongoHost},
	}
	session, err = mgo.DialWithInfo(di)
	if err != nil {
		fmt.Println("Unable to connect to database", err)
		os.Exit(1)
	}
	sessionCopy := session.Copy()
	defer sessionCopy.Close()
	c := sessionCopy.DB(AppConfig.Database).C("users")
	index := mgo.Index{
		Key:    []string{"email"},
		Unique: true,
	}
	err = c.EnsureIndex(index)
	if err != nil {
		fmt.Println("Unable to create index", err)
		os.Exit(1)
	}
}
