package bootstrapper

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

type config struct {
	LogLevel                    int
	Server, MongoHost, Database string
}

var AppConfig config

func init() {
	viper.SetConfigName("app")
	viper.AddConfigPath("config")
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("Config file not found.", err)
		os.Exit(1)
	}
	AppConfig = config{}
	AppConfig.LogLevel = viper.GetInt("development.LogLevel")
	AppConfig.Server = viper.GetString("development.Server")
	AppConfig.MongoHost = viper.GetString("development.MongoHost")
	AppConfig.Database = viper.GetString("development.Database")
}

func StartUp() {
	// TODO: logger initialize
	initializeDatabase()
}
