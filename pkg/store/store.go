package store

import (
	"gopkg.in/mgo.v2"

	"user/pkg/bootstrapper"
)

type Store struct {
	MongoSession *mgo.Session
}

func (s *Store) Close() {
	s.MongoSession.Close()
}

func (s *Store) Collection(n string) *mgo.Collection {
	return s.MongoSession.DB(bootstrapper.AppConfig.Database).C(n)
}

func NewStore() *Store {
	session := bootstrapper.GetSession().Copy()
	store := &Store{
		MongoSession: session,
	}
	return store
}
