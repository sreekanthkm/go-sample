package store

import (
	"user/pkg/model"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type UserStore struct {
	C *mgo.Collection
}

func (ut UserStore) Create(user model.User) error {
	user.ID = bson.NewObjectId()
	err := ut.C.Insert(user)
	return err
}

func (ut UserStore) GetByMail(email string) (model.User, error) {
	var user model.User
	err := ut.C.Find(bson.M{"email": email}).One(&user)
	if err != nil {
		return model.User{}, err
	}
	return user, nil
}

func (ut UserStore) GetAll() ([]model.User, error) {
	var users []model.User
	err := ut.C.Find(bson.M{}).All(&users)
	return users, err
}
