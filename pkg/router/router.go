package router

import "github.com/gorilla/mux"

func InitRoutes() *mux.Router {
	router := mux.NewRouter().StrictSlash(false)
	s := router.PathPrefix("/api").Subrouter()
	s = SetUserRoutes(s)
	s = SetDetailRoutes(s)
	return router
}
