package router

import (
	"github.com/gorilla/mux"
	"user/pkg/controller"
)

func SetUserRoutes(router *mux.Router) *mux.Router {
	s := router.PathPrefix("/user").Subrouter()
	s.HandleFunc("/login", controller.Login).Methods("POST")
	s.HandleFunc("/register", controller.Register).Methods("POST")
	return router
}
