package router

import (
	"github.com/gorilla/mux"
	"user/pkg/controller"
)

func SetDetailRoutes(router *mux.Router) *mux.Router {
	s := router.PathPrefix("/detail").Subrouter()
	s.HandleFunc("", controller.GetAll).Methods("GET")
	return router
}
