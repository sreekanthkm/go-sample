package controller

import (
	"encoding/json"
	"fmt"
	"net/http"

	"golang.org/x/crypto/bcrypt"
	"user/pkg/model"
	"user/pkg/store"
)

func Login(w http.ResponseWriter, r *http.Request) {
	// TODO: Handle input validation
	u := model.User{}
	err := json.NewDecoder(r.Body).Decode(&u)
	if err != nil {
		fmt.Println("Unable to parse body")
	}
	s := store.NewStore()
	defer s.Close()
	c := s.Collection("users")
	userStore := store.UserStore{C: c}
	user, err := userStore.GetByMail(u.Email)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(u.Password))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func Register(w http.ResponseWriter, r *http.Request) {
	// TODO: Handle input validation
	u := model.User{}
	err := json.NewDecoder(r.Body).Decode(&u)
	if err != nil {
		fmt.Println("Unable to parse body")
	}
	s := store.NewStore()
	defer s.Close()
	c := s.Collection("users")
	userStore := store.UserStore{C: c}
	user := model.User{
		FirstName: u.FirstName,
		LastName:  u.LastName,
		Email:     u.Email,
	}
	pass, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	w.Header().Set("Content-Type", "application/json")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	user.Password = string(pass)
	err = userStore.Create(user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(http.StatusCreated)
}
