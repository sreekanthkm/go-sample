package controller

import (
	"encoding/json"
	"net/http"

	"user/pkg/store"
)

func GetAll(w http.ResponseWriter, r *http.Request) {
	s := store.NewStore()
	defer s.Close()
	c := s.Collection("users")
	userStore := store.UserStore{C: c}
	users, err := userStore.GetAll()
	w.Header().Set("Content-Type", "application/json")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	j, err := json.Marshal(users)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}
