package main

import (
	"fmt"
	"net/http"
	"time"

	"user/pkg/bootstrapper"
	"user/pkg/router"
)

func main() {
	bootstrapper.StartUp()
	r := router.InitRoutes()
	server := &http.Server{
		Handler:      r,
		Addr:         bootstrapper.AppConfig.Server,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	fmt.Println("Listening in ", bootstrapper.AppConfig.Server)
	err := server.ListenAndServe()
	fmt.Println(err)
	//TODO: use go routine and handle signals using channels
}
